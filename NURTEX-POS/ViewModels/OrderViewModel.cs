﻿using POS.Models;

namespace NURTEX_POS.ViewModels
{
    public class OrderViewModel
    {
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public double SellingPrice { get; set; }
        public double Discount { get; set; }
        public double Total { get; set; }
        public static OrderViewModel Convert(Order order)
        {
            return new OrderViewModel()
            {
                ProductName = order.Product.Name,
                Quantity = order.Quantity,
                SellingPrice = order.SellingPrice,
                Discount = order.Discount,
                Total = order.Total
            };
        }
    }
}
