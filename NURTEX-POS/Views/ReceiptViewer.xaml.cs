﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using NURTEX_POS.Reports;
using NURTEX_POS.ViewModels;
using POS.Models;

namespace NURTEX_POS.Views
{
    /// <summary>
    /// Interaction logic for ReceiptViewer.xaml
    /// </summary>
    public partial class ReceiptViewer : Window
    {
        //private AppOperations appOperations = new AppOperations();
        public ReceiptViewer(Sale sale)
        {
            InitializeComponent();

            viewer.ToggleSidePanel = SAPBusinessObjects.WPF.Viewer.Constants.SidePanelKind.None;

            var orders = new List<OrderViewModel>();
            foreach (var o in sale.Orders)
                orders.Add(OrderViewModel.Convert(o));

            var receipt = new Receipt();
            receipt.Load("Receipt.rpt");
            receipt.SetDataSource(orders);

            receipt.SetParameterValue("billNo", sale.Id);
            receipt.SetParameterValue("date", sale.PurcheseDateTime);
            receipt.SetParameterValue("customerName", sale.CustomerName);
            receipt.SetParameterValue("customerAddress", sale.Address);
            receipt.SetParameterValue("totalAmmount", sale.GetTotalAmmount());
            receipt.SetParameterValue("productDiscount", sale.Orders.Sum(order => order.Product.UnitSellingPrice - order.SellingPrice));
            if(sale.DiscountToken != null) receipt.SetParameterValue("tokenDiscount", sale.GetTotalAmmount() * (sale.DiscountToken.Percentage / 100));
            else receipt.SetParameterValue("tokenDiscount", 0);

            viewer.ViewerCore.ReportSource = receipt;
        }
    }
}
