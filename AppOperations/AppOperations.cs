﻿using POS.BusinessLogics;
using POS.DataAccess;
using POS.Models;

namespace AppOperations
{
    public static class AppOperations
    {
        private static User _currentUser = null;
        private static UserLogics _userLogics = new UserLogics();

        public static bool CheckConnection()
        {
            return NurTexDbContext.GetContext().Database.Exists();
        }

        public static bool Login(string username, string password)
        {
            if (!_userLogics.Authenticate(username, password)) return false;
            _currentUser = _userLogics.GetUserByUsername(username);
            return true;
        }

        public static User GetCurrentUser()
        {
            return _currentUser;
        }

        public static void Logout()
        {
            _currentUser = null;
        }
    }
}
